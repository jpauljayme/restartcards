FROM 10.233.132.114/ucpadmin/openjdk:8u181

ADD target/demo-0.1-SNAPSHOT.jar app.jar

EXPOSE 8080

CMD java $JAVA_OPTS -jar /app.jar $APP_OPTS
